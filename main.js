/**
 * An array of status effect icons which can be applied to Tokens
 * @type {Array}
 */
CONFIG.statusEffects = [
    {
      id: "dead",
      label: "Dead",
      icon: "modules/vrd-conditions-5e/icons/svg/skull.svg"
    },
    {
      id: "deathdoor",
      label: "Death's Door",
      icon: "modules/vrd-conditions-5e/icons/coma.png"
    },
    {
      id: "unconscious",
      label: "Unconcious",
      icon: "modules/vrd-conditions-5e/icons/svg/unconscious.svg"
    },
    {
      id: "sleep",
      label: "Sleep",
      icon: "modules/vrd-conditions-5e/icons/svg/sleep.svg"
    },
    {
      id: "stunned",
      label: "Stunned",
      icon: "modules/vrd-conditions-5e/icons/knockout.png"
    },
    {
      id: "prone",
      label: "Knocked Prone",
      icon: "modules/vrd-conditions-5e/icons/svg/falling.svg"
    },
    {
      id: "dodge",
      label: "Dodging",
      icon: "modules/vrd-conditions-5e/icons/dodging.png"
    },



    {
      id: "grappled",
      label: "Grappled",
      icon: "modules/vrd-conditions-5e/icons/grab.png"
    },
    {
      id: "disarmed",
      label: "Disarmed",
      icon: "modules/vrd-conditions-5e/icons/drop-weapon.png"
    },
    {
      id: "restrained",
      label: "Restrained",
      icon: "modules/vrd-conditions-5e/icons/svg/net.svg"
    },
    {
      id: "cuffed",
      label: "Cuffed",
      icon: "modules/vrd-conditions-5e/icons/manacles.png"
    },
    {
      id: "immobilized",
      label: "Immobilized",
      icon: "modules/vrd-conditions-5e/icons/nailed-foot.png"
    },
    {
      id: "disoriented",
      label: "Disoriented",
      icon: "modules/vrd-conditions-5e/icons/stoned-skull.png"
    },
    {
      id: "disintegrated",
      label: "Disintegrated",
      icon: "modules/vrd-conditions-5e/icons/disintegrate.png"
    },
	
	
	
    {
      id: "acceleration",
      label: "Haste",
      icon: "modules/vrd-conditions-5e/icons/sprint.png"
    },
    {
      id: "slowed",
      label: "Slowed",
      icon: "modules/vrd-conditions-5e/icons/snail.png"
    },
    {
      id: "crippled",
      label: "Crippled",
      icon: "modules/vrd-conditions-5e/icons/arm-sling.png"
    },
    {
      id: "magicpalm",
      label: "Magic Palm",
      icon: "modules/vrd-conditions-5e/icons/magic-palm.png"
    },
    {
      id: "paralyzed",
      label: "Paralyzed",
      icon: "modules/vrd-conditions-5e/icons/internal-injury.png"
    },
    {
      id: "overheated",
      label: "Extreme Heat",
      icon: "modules/vrd-conditions-5e/icons/thermometer-hot.png"
    },
    {
      id: "freezing",
      label: "Extreme Cold",
      icon: "modules/vrd-conditions-5e/icons/thermometer-cold.png"
    },



    {
      id: "blinded",
      label: "Blinded",
      icon: "modules/vrd-conditions-5e/icons/svg/blind.svg"
    },
    {
      id: "deafened",
      label: "Deafened",
      icon: "modules/vrd-conditions-5e/icons/svg/deaf.svg"
    },
    {
      id: "petrified",
      label: "Petrified",
      icon: "modules/vrd-conditions-5e/icons/stone-block.png"
    },
    {
      id: "combatres",
      label: "Raised at Combat",
      icon: "modules/vrd-conditions-5e/icons/heart-wings.png"
    },
    {
      id: "exhausted",
      label: "Exhausted",
      icon: "modules/vrd-conditions-5e/icons/oppression.png"
    },
    {
      id: "climb",
      label: "Climbing",
      icon: "modules/vrd-conditions-5e/icons/mountain-climbing.png"
    },
    {
      id: "resting",
      label: "Resting",
      icon: "modules/vrd-conditions-5e/icons/life-bar.png"
    },



    {
      id: "fear",
      label: "Frightened",
      icon: "modules/vrd-conditions-5e/icons/screaming.png"
    },
    {
      id: "horrified",
      label: "Horrified",
      icon: "modules/vrd-conditions-5e/icons/dread.png"
    },
    {
      id: "targeted",
      label: "Targeted",
      icon: "modules/vrd-conditions-5e/icons/targeting.png"
    },
    {
      id: "bloodloss",
      label: "Blood Loss",
      icon: "modules/vrd-conditions-5e/icons/svg/blood.svg"
    },
    {
      id: "advantage",
      label: "Vantage",
      icon: "modules/vrd-conditions-5e/icons/arrow-scope.png"
    },
    {
      id: "ill",
      label: "Diseased",
      icon: "modules/vrd-conditions-5e/icons/virus.png"
    },
    {
      id: "ritual",
      label: "Ritual",
      icon: "modules/vrd-conditions-5e/icons/meditation.png"
    },	
	

    {
      id: "darkvision",
      label: "Vision Buff",
      icon: "modules/vrd-conditions-5e/icons/svg/eye.svg"
    },
    {
      id: "ambush",
      label: "Ambush",
      icon: "modules/vrd-conditions-5e/icons/ninja-mask.png"
    },
    {
      id: "invisible",
      label: "Invisible",
      icon: "modules/vrd-conditions-5e/icons/invisible.png"
    },
    {
      id: "buffed",
      label: "Buffed",
      icon: "modules/vrd-conditions-5e/icons/strong.png"
    },
    {
      id: "enraged",
      label: "Enraged",
      icon: "modules/vrd-conditions-5e/icons/enrage.png"
    },
    {
      id: "magicbuffed",
      label: "Magic Buff",
      icon: "modules/vrd-conditions-5e/icons/boomerang-sun.png"
    },
    {
      id: "encumbered",
      label: "Encumbered",
      icon: "modules/vrd-conditions-5e/icons/swap-bag.png"
    },


    {
      id: "concentration",
      label: "Concentration",
      icon: "modules/vrd-conditions-5e/icons/clockwork.png"
    },
    {
      id: "flight",
      label: "Flight",
      icon: "modules/vrd-conditions-5e/icons/spiky-wing.png"
    },
    {
      id: "cursed",
      label: "Cursed",
      icon: "modules/vrd-conditions-5e/icons/cursed-star.png"
    },
    {
      id: "hexed",
      label: "Hex",
      icon: "modules/vrd-conditions-5e/icons/dripping-star.png"
    },
    {
      id: "immunity",
      label: "Poison Protection",
      icon: "modules/vrd-conditions-5e/icons/potion-ball.png"
    },
    {
      id: "dehydrated",
      label: "Dehydrated",
      icon: "modules/vrd-conditions-5e/icons/magic-potion.png"
    },
    {
      id: "hungry",
      label: "Hungry",
      icon: "modules/vrd-conditions-5e/icons/stomach.png"
    },	


    {
      id: "hpup",
      label: "Health Up",
      icon: "modules/vrd-conditions-5e/icons/svg/regen.svg"
    },
    {
      id: "hpdown",
      label: "Health Down",
      icon: "modules/vrd-conditions-5e/icons/svg/degen.svg"
    },
    {
      id: "inspired",
      label: "Inspiration",
      icon: "modules/vrd-conditions-5e/icons/music-spell.png"
    },
    {
      id: "magiceffect",
      label: "Magic Effect",
      icon: "modules/vrd-conditions-5e/icons/svg/daze.svg"
    },
    {
      id: "holymagic",
      label: "Holy Magic",
      icon: "modules/vrd-conditions-5e/icons/svg/angel.svg"
    },
    {
      id: "aura",
      label: "Aura Effect",
      icon: "modules/vrd-conditions-5e/icons/beams-aura.png"
    },
    {
      id: "surrounder",
      label: "Surrounder",
      icon: "modules/vrd-conditions-5e/icons/shield-opposition.png"
    },

	
	
    {
      id: "fire",
      label: "Fire",
      icon: "modules/vrd-conditions-5e/icons/svg/fire.svg"
    },
    {
      id: "ice",
      label: "Ice",
      icon: "modules/vrd-conditions-5e/icons/snowflake-2.png"
    },
    {
      id: "lightning",
      label: "Lightning",
      icon: "modules/vrd-conditions-5e/icons/svg/lightning.svg"
    },
    {
      id: "acid",
      label: "Acid",
      icon: "modules/vrd-conditions-5e/icons/acid.png"
    },
    {
      id: "poisoned",
      label: "Poisoned",
      icon: "modules/vrd-conditions-5e/icons/poison-bottle.png"
    },
    {
      id: "potionbuffed",
      label: "Potion Buff",
      icon: "modules/vrd-conditions-5e/icons/standing-potion.png"
    },
    {
      id: "surrounded",
      label: "Surrounded",
      icon: "modules/vrd-conditions-5e/icons/surrounded-shield.png"
    },



    {
      id: "shield",
      label: "Armor Up",
      icon: "modules/vrd-conditions-5e/icons/shield.png"
    },
    {
      id: "fireshield",
      label: "Fire Shield",
      icon: "modules/vrd-conditions-5e/icons/svg/fire-shield.svg"
    },
    {
      id: "iceshield",
      label: "Ice Shield",
      icon: "modules/vrd-conditions-5e/icons/svg/ice-shield.svg"
    },
    {
      id: "mageshield",
      label: "Magic Shield",
      icon: "modules/vrd-conditions-5e/icons/svg/mage-shield.svg"
    },
    {
      id: "holyshield",
      label: "Holy Shield",
      icon: "modules/vrd-conditions-5e/icons/svg/holy-shield.svg"
    },
    {
      id: "diceeffect",
      label: "Unstable Buff",
      icon: "modules/vrd-conditions-5e/icons/svg/holy-shield.svg"
    },
    {
      id: "armordown",
      label: "Armor Down",
      icon: "modules/vrd-conditions-5e/icons/shield-disabled.png"
    },	
    {
      id: "noshield",
      label: "Vulnerable",
      icon: "modules/vrd-conditions-5e/icons/cracked-shield.png"
    },
	
	
	
    {
      id: "whiteflag",
      label: "White Flag",
      icon: "modules/vrd-conditions-5e/icons/whiteflag.png"
    },
    {
      id: "redflag",
      label: "Red Flag",
      icon: "modules/vrd-conditions-5e/icons/redflag.png"
    },
    {
      id: "greenflag",
      label: "Green Flag",
      icon: "modules/vrd-conditions-5e/icons/greenflag.png"
    },
    {
      id: "blueflag",
      label: "Blue Flag",
      icon: "modules/vrd-conditions-5e/icons/blueflag.png"
    },
    {
      id: "yellowflag",
      label: "Yellow Flag",
      icon: "modules/vrd-conditions-5e/icons/yellowflag.png"
    },
    {
      id: "purpleflag",
      label: "Purple Flag",
      icon: "modules/vrd-conditions-5e/icons/purple-flag.png"
    },
    {
      id: "brownflag",
      label: "Brown Flag",
      icon: "modules/vrd-conditions-5e/icons/brown-flag.png"
    },		
	
	
];


  
